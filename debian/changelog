debian-multimedia (0.10) unstable; urgency=medium

  * Source only upload (Closes: #978047)

 -- Ross Gammon <rossgammon@debian.org>  Fri, 25 Dec 2020 16:09:36 +0100

debian-multimedia (0.9) unstable; urgency=medium

  [ Ross Gammon ]
  * git-core no longer built by git source package - removed
  * python3-ecasound now in archive - remove WNPP info
  * python3-pyknon now in the archive - remove WNPP info
  * Photoflow now in Salsa - Add Vcs info
  * And photoflow is in photography task too!
  * Add spleeter as a potential package
  * Add shotcut to video task
  * Add miniaudio to recorders & players
  * Add gir1.2-gexiv2-0.10 & libgexiv2-dev to devel task
  * Add Vcs-Browser field to photoflow

  [ Balint Reczey ]
  * Only Suggest: kodi and reverse dependencies

  [ Olivier Humbert ]
  * Update d/control (typo disk/disc)

  [ Ross Gammon ]
  * Add wand (Python module for ImageMagic) to devel task
  * Add abr2gbr to photography task
  * Add zita-dc1 to audio-utilities task
  * Add elektroid to samplers task
  * Add lsp-plugins to audio-plugins task
  * Add RFP for canon-capture to photography task
  * Add fft-spectra RFP to audio-utilities task
  * Add ITP bug for re-introducing hydrogen
  * Add shotwell to photography task
  * Add simple-scan to graphics task
  * Add audmes to audio-utilities task
  * Add dvbtune to video task
  * Add audiolink to audio-utilities task
  * Add studio-controls RFP to jack task
  * Add WNPP bug to spleeter
  * Add gnome-metronome to audio-utilities task
  * Add photoprint ITP to photography task
  * Add vguitar ITP to MIDI task
  * Add libaperture-0-dev to devel task
  * Add lazpaint to graphics task
  * Add mp3report to audio-utilities task
  * Add osmid to midi task
  * Add RFP bug for new-session-manager
  * Rename new-session-manager as called un Ubuntu
  * Add raysession as possible package to the mixing task
  * Photoflow now in archive, remove superfluous information
  * Photoflow now in archive, remove superfluous information also in graphics task
  * Fix syntax error for vguitar entry
  * libqt4-dev no longer in archive, replace with qtbase5-dev (Closes: #953384)
  * Apply Olivier's change to the task file

 -- Ross Gammon <rossgammon@debian.org>  Sat, 14 Nov 2020 16:06:20 +0100

debian-multimedia (0.8) unstable; urgency=medium

  * Generate new package lists
  * libjuce-dev no longer built by juce (Closes: #885799)
  * qwinff and taptempo now in the archive
  * libavg removed from testing & unstable
  * butt now in the archive
  * python-csoundac no longer built by csound source package
  * python3-jack-client and libimgscalr-java now in the archive
  * Drop libraries no longer built by ffmpeg
  * Add latest upstream information for python3-pyknon
  * Added information about python3-libmapper in latest upstream release
  * python3-ecasound: Add info about upstream work on Python 3 support
  * Fix package name for ableton-link
  * libavg removed from testing & unstable
  * rgain removed from testing & unstable
  * img2pdf now in the archive
  * qosmic now in the archive
  * dsmidiwifi is now in the archive
  * mpdas now in the archive
  * mopidy-youtube has been removed from testing & unstable
  * libav-tools no longer built by ffmpeg (Closes: #895917)
  * qwinff now in the archive
  * Remove avidemux Alioth links & add upstream homepage
  * Add ITP for vst3sdk to audio-plugins
  * Add new fst-dev package to devel task
  * Add streamlink to devel & video tasks
  * Add ITP for sk1 to graphics task
  * Add new mopidy-mpd package to players
  * Add new mopidy-local package to players
  * Add vokoscreen & vokoscreen-ng to video task
  * Add osc2midi ITP to midi task
  * Add new libmedialibrary-dev package to devel tasks
  * python3-grapefruit bug fixed in NMU without closing in changelog
   (Closes: #934125)
  * Add ${misc:Depends} to multimedia-tasks to fix lintian Warning
  * Bump debhelper compat to 12
  * Bump standards version, no changes required

 -- Ross Gammon <rossgammon@debian.org>  Sun, 05 Jan 2020 18:40:01 +0100

debian-multimedia (0.7+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch all python2 relationships to python3, downgrading to Suggests for
    packages which still dont have a python3 counterpart; Closes: #945633
  * Generate files

 -- Sandro Tosi <morph@debian.org>  Tue, 10 Dec 2019 11:40:31 -0500

debian-multimedia (0.7) unstable; urgency=medium

  [ Ross Gammon ]
  * libmysofa, mediagoblin & yasw now in the archive
  * Add some recent ITP/RFP/RFS & O of multimedia type packages
  * Add libuvc-dev to devel task
  * Add libmpd & wmin, currently orphaned
  * Add a couple more orphaned streamers to broadcasting
  * Add more packages from the WNPP pseudo package
  * Move dleyna-connector-dbus & dleyna-core to devel as they only have
    libraries
  * Remove dino, no longer in archive (Closes: #879783)
  * Add newish snapcast software to broadcasting
  * Remove libjuce-dev which is no longer part of the juce package
  * Demote libav-tools to a "Suggests:"

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ IOhannes m zmölnig ]
  * removed pd-extended
  * pd-jsusfx is in the archive; camomile is not

 -- Ross Gammon <rossgammon@debian.org>  Sat, 09 Jun 2018 15:22:20 +0200

debian-multimedia (0.6) unstable; urgency=medium

  [ IOhannes m zmölnig ]
  * Added 'ardour' to the 'mixing' task
  * Replaced 'pd' packages with meta-package 'multimedia-puredata' in
    'soundsynthesis' task
  * Added a SuperCollider task
  * Added a csound task
  * Replaced sc-packages with 'multimedia-supercollider' in
    'soundsynthesis' task
  * Replaced csound-packages with 'multimedia-csound' in
    'soundsynthesis' task
  * Added Pd-development packages
  * Added ableton-live-dev
  * Added libambix packages
  * pd-lib-builder, pd-flext, libambix and ableton-link have been
    accepted into unstable
  * Added ableton-link
  * added WNPP bug-numbers for some pd-flext-related packages
  * Added pd-vasp and pd-upp
  * pd-autopreset has been accepted into unstable.
  * pd-pool/pd-py/pd-xsample have been accepted into Debian
  * pd-punish is included in pd-iemutils
  * pd-abstractions and pd-vasp have some issues
  * pd-upp now in the archive

  [ Ross Gammon ]
  * Preparing a new upload
  * Update task descriptions & d/control using make dist
  * Use Debian email address
  * Bump standards version, switched to https for copyright format
    & switched priority to optional
  * libopenscenegraph-3.4 now in the archive
  * Add libyami & libyami-utils to devel task
  * Add libopenmpt ITP to devel task
  * Add qstopmotion ITP to animation task
  * Add cdrdao to audio-utilities & recording tasks
  * Add xviewer, xviewer-plugins and xplayer ITPs
  * Add itktools to graphics & photography tasks
  * Add pd-autopreset, pd-bandlimited, pd-punish ITPs to puredata task
  * Fix package name libambix-tools > libambix-utils
  * Fix syntax error & add Vcs URI for qstopmotion
  * Fix syntax error in audio-utilities task
  * Fix syntax error on photography task
  * Drop mopidy-podcast-gpodder, removed from archive
  * pd-jsusfx now in the archive & fixed pd-flext-dev package name
  * Add digikam to photgraphy task & potential RFP photo enhancer
  * Add spek to audio-utilities task
  * Add nama, vamps & fische to relevant tasks
  * Libyami now in the archive
  * qstopmotion now in the archive, kodi-eventclient-j2me dropped
  * Add iannix ITP
  * Add pragha RFP
  * Fix syntax error & drop pragha which was already listed!
  * Add libmysofa to devel & audio-utilities (currently in NEW)
  * Add gerbera ITP
  * Add svgpp ITP
  * Add posterazor to graphics & photography
  * Add libbabl-dev to devel task
  * Add libgegl-dev & gegl packages
  * Add libimgscalr-java to devel task
  * Added yasw ITP
  * Add libavg to audio-utils, video & devel
  * Fix syntax errors & remove info for iannix - now in archive

  [ James Cowgill ]
  * Remove mplayer2 which no longer exists

 -- Ross Gammon <rossgammon@debian.org>  Mon, 07 Aug 2017 22:24:10 +0200

debian-multimedia (0.5) unstable; urgency=medium

  [ Ross Gammon ]
  * Prepare a new release
  * Mark tasks that should be installed during blend installation
    (Closes: #825186)
  * Add pulseaudio-dlna ITP to broadcasting task
  * Add NEW sc3-plugin packages
  * Add libqpsd ITP to devel task
  * Add openshot-qt and library ITPs
  * Add link to unofficial debian packages for sayonara
  * Add nordlicht ITP to video task
  * pnmixer is now in the archive, remove package information
  * pd-purest-json is now in the archive, remove package information
  * Add Sonic-Pi dependencies to devel task
  * Add some Cinema RFPs
  * Add mopidy-somafm ITP to players task
  * Add snd packages to audio utilities task
  * Add flif & mopidy-internetarchive ITPs to relevant tasks
  * pd-iem, pd-iemutils, pd-nusmuk & nordlicht now in the archive
  * pd-puremapping now in the archive, remove extra package info
  * Add ITPs for baka-mplayer, castnow, and dcadec
  * Mediaconch & drumgizmo now in the archive
  * Add NEW sonic-pi to soundsynthesis task
  * Add python-tunigo to devel task
  * pd-extendedview now in the archive, removed extra package information
  * pulseaudio-dlna now in the archive, removing extra package info
  * Use binary package names for juce
  * Add packaging Vcs for cinelerra-cv, packaging was started
  * Open Broadcasting Software (obs) is NEW
  * Add sayonara VCS, and a note that openscengraph-3.4 is NEW

  [ IOhannes m zmölnig ]
  * removed pd-iem-bin-ambi
  * added pd-iemutils
  * added pd-purest-json
  * sorted in pd-extendedview
  * added JUCE
  * added more puredata wishlist packages
  * fix short description for pd-oscbank
  * added some more wishlist packages to puredata
  * pd-kollabs now in unstable
  * juce now in the archive
  * added pd-jsusfx
  * added ITP:ambix-plugins to "ambisonics"
  * "pysoundfile" has been in the archive for some time...
  * Added ITP:libambix to "devel" section
  * Added pd-acre
  * Added pd-ehu
  * Added camomila
  * fixed typo: pd-eho -> pd-ehu
  * Fixed typo: camomila -> camomile
  * Fixed typo: "Non DAW Studio"
  * Note that pd-iem-bin-ambi is likely to be obsolete
  * ambidecodertoolbox
  * 'tab' has been renamed to 'lute-tab'

 -- Ross Gammon <rossgammon@mail.dk>  Sun, 05 Jun 2016 21:35:07 +0200

debian-multimedia (0.4) unstable; urgency=medium

  [ Ross Gammon ]
  * Add NEW linuxptp package
  * Add deken to soundsynthesis task
  * Add ffmpeg back into video & devel tasks
  * Add new timgm6mb-soundfont package to midi task
  * Fix Vcs browser URL
  * Add libmusicbrainz to the devel task
  * Add qsequencer to midi, recording & drums tasks
  * Add mopidy-local-sqlite to players task
  * Add NEW pd-iemguts package to puredata task
  * Add sndio to jack and devel tasks
  * pd-syslog is NEW, added to puredata task
  * Update pd-rtclib package name, was pt-rtc
  * deken now in the archive, removed package information
  * Expand the timestretching task to be audio-utilities
  * pd-hexloader now in the archive, WNPP details deleted
  * Added ecasound to audio-utilities & devel tasks
  * Create a new Broadcasting task and move stuff there
  * Add midicsv to midi task
  * Remove package information from pd-tclpd, now in the archive
  * sndio-xtools no longer part of source package
  * Rename timestretching task to audio-utilities
  * Create new photography task and copy/move packages from graphics/video
  * Add NEW kodi-pvr-vdr-vnsi package to broadcasting, photography & video
  * Add more photography packages from the Debian PhotoTools Maintainers
  * Fix openmageio-tools using package name not source package
  * Add NEW kodi-data package
  * Add a new animation task with packages already in graphics & video
  * Add NEW hydrogen-drumkits-effect package as a suggests
  * libav package has been removed, update package names for ffmpeg
  * gnome-mpv now in the archive
  * Remove extra info for dgedit as now in archive & fix syntax error
  * Remove extra info for pd-gil, now in the archive
  * Add new kodi-pvr-hts addon to broadcasting & video
  * Add kodi-pvr-argustv to video & broadcasting tasks
  * Add new kodi addons to video & brodcasting tasks
  * Add new pvr-hdhomerun package to video & broadcasting tasks
  * Apply patch to fix typo and remove duplicates.
    Thanks to Andreas Tille
  * Apply patch from Ubuntu libdvbpsi9 -> libdvbpsi-dev.
    Thanks to Matthias Klose
  * Regenerate control and desc file

  [ IOhannes m zmölnig ]
  * added pd-iemambi to "ambisonics" task
  * Removed trailing whitespace
  * moved pd's graphics packages from "soundsynthesis" to "graphics"
  * Added 'puredata' task
  * pd-rtclib now in the archive
  * pd-mrpeach provides multiple binary packages
  * added more pd-packages found in task "soundsynthesis"
  * Removed pd-packages unrelated to "soundsynthesis"
  * pd-creb now in the archive

  [ Kaj Ailomaa ]
  * removing phat packages, as phat has been removed from Debian archive

 -- Ross Gammon <rossgammon@mail.dk>  Fri, 08 Jan 2016 16:34:34 +0100

debian-multimedia (0.3) unstable; urgency=low

  [ Felipe Sateler ]
  * Add Vcs-* fields
  * qutecsound was dropped, csoundqt is the new name
  * Regenerate control and desc file


  [ Reinhard Tartler ]
  * Cleanup whitespace in task definitions
  * Update package name for Libav-related utilities (Closes: #722256)
  * gcdmaster is no longer built by the cdrdao package

  [ Andreas Tille ]
  * The task name is no repetition of the metapackage. Do not repeat
    multimedia-.

  [ Emmanouil Kiagias ]
  * added dependency_data/ directory containing the dependency json
    files for releases 0.2 and 0.3, files needed for new blends-dev

  [ Ross Gammon ]
  * Update ambisonics task with latest information (Closes: #766091)
  * Update all tasks to latest Debian Multimedia package set
  * Remove packages no longer in the archive
  * Add recent NEW packages to tasks
  * Drop non-multimedia development packages to "Suggests"
  * Add some relevant ITPs & RFPs
  * Add myself to Uploaders
  * Update Vcs URL to use cgit instead of gitweb
  * Bump standards version, no changes required
  * Fix lintian warnings

  [ Ruben Undheim ]
  * Add sfarkxtc to the midi task
  * Add vmpk to midi task

  [ Tiago Bortoletto Vaz ]
  * Fix small typo in musiciantool task.

 -- Ross Gammon <rossgammon@mail.dk>  Mon, 01 Jun 2015 18:01:37 -0300

debian-multimedia (0.2) unstable; urgency=low

  * New version for debian-multimedia team
    - Rename all packages to remove the openstudio brand
    - Change maintainer to the multimedia team
    - Remove out-of-scope metapackages
    - Merge related metapackages
      + *-plugins into audio-plugins
      + synth and soundsynthesis
      + trackers and midi
      + musicnotation and composing into musiciantools
  * Remove all dev packages except in -devel task
  * Fix various package names
  * Remove stale files from example blends project
  * Add Reinhard to Uploaders
  * Add multimedia-tasks package
  * Add copyright file
  * Shorten description lines to < 80 chars
  * Add source format version
  * Bump standards-version

 -- Felipe Sateler <fsateler@debian.org>  Mon, 20 May 2013 10:05:07 -0400

debian-multimedia (0.1) lucid; urgency=low

  * Initial release.

 -- peerjam <linuxpeerjam@gmail.com>  Sun, 31 Oct 2010 10:02:57 +0100
