Task: Ambisonics
Description: Packages for working with ambisonics (3D surround sound)
 This metapackage will install packages you need when working with
 ambisonics.
Install: true

Depends: ambdec

Depends: amb-plugins

Suggests: ambix-plugins
WNPP: 817834
Homepage: http://www.matthiaskronlachner.com/?p=2015
License: GPL-2+
Pkg-Description: ambiX Ambisonics plug-in suite
 This is a set of cross-platform Ambisonics VST, LV2 plug-ins with variable
 order for use in Digital Audio Workstations like Ardour or as JACK standalone
 applications.
 .
 The plug-in suite use the ambiX convention (ACN channel order, SN3D
 normalization, full periphony (3D)). These plug-ins use a recursive
 implementation of the spherical harmonics, therefore the maximum Ambisonic
 order is defined at compile time. The practical maximum order is rather
 defined by the hosts maximum channel count or your CPU power.

Depends: ardour

Depends: ardour3

Depends: jackd

Depends: qjackctl

Depends: japa

Depends: jconvolver

Depends: jconvolver-config-files

Depends: drc

Depends: aliki

Depends: zita-rev1

Suggests: non-mixer
Homepage: http://non.tuxfamily.org/
WNPP: 681576
License: GPL-2+
Pkg-URL: http://non.tuxfamily.org/wiki/BinaryPackagesAndBuilds
Pkg-Description: live mixer with effects plugin hosting and ambisonics
 .
 The Non DAW Studio is a modular system composed of four main parts consisting
 of the Non Timeline, the Non Mixer, the Non Sequencer, and the Non Session
 Manager.
 .
 The Non DAW Studio is a complete studio that is fast and light enough to run
 on low-end hardware.
 .
 This is the Non Mixer package.

Depends: soundscaperenderer

Depends: tetraproc

Depends: pd-iemambi

Depends: pd-iem-bin-ambi
Pkg-Description: binaural spatial audio for Pure Data using Ambisonics
 This library includes objects to render binaural 2D- and 3D-soundfields
 (to be played back via headphones) within Pure Data, using the
 Higher-Order Ambisonics approach.
#Note: Upstream decided to integrate pd-iem-bin-ambi into pd-iemambi,
#      though they haven't released anything yet
Homepage: http://ambisonics.iem.at/
License: GPL
WNPP: 603178

Depends: ambidecodertoolbox
Homepage: https://bitbucket.org/ambidecodertoolbox/adt
Pkg-Description: a collection of tools for creating Ambisonic decoders
 The Ambisonic Decoder Toolbox is a collection of MATLAB and GNU Octave functions
 for creating Ambisonic Decoders. Currently, it implements:
  - the AllRAD design technique
  - the inversion or mode-matching
  - truncated mode-matching
  - linear combinations of the former two
  - constant energy
  - Slepian function basis (EPAD)
License: AGPL3

Depends: libambix-utils, libambix-dev, pd-ambix

Depends: iem-plugin-suite-standalone, iem-plugin-suite-vst
