Task: MIDI
Description: MIDI related packages
 This metapackage will install packages useful for MIDI, including trackers,
 MIDI synths and virtual keyboards.
Install: true

Depends: qtractor

Depends: a2jmidid

Depends: jack-keyboard

Depends: vkeybd

Depends: qsynth

Suggests: aubio-tools

Depends: fluidsynth

Depends: fluid-soundfont-gm

Suggests: alsa-utils

Depends: yoshimi

Depends: seq24

Depends: gmidimonitor

Depends: qmidiarp

Depends: qmidiroute

Depends: jack-smf-utils
Pkg-Description: two jack utilities for playing & recording midi 
 This package provides a set of two utilities: jack-smf-player and
 jack-smf-recorder. The purpose of these packages is to play and
 record MIDI streams from/to Standard MIDI Files (i.e. the files
 with .mid extension) using JACK MIDI.
Homepage: http://sourceforge.net/projects/jack-smf-utils/
WNPP: 609968
License: BSD

Suggests: kmidimon

Depends: abcmidi

Suggests: rosegarden

Suggests: mididings

Suggests: muse

Suggests: non-sequencer
Homepage: http://non.tuxfamily.org/
WNPP: 681576
License: GPL-2+
Pkg-URL: http://non.tuxfamily.org/wiki/BinaryPackagesAndBuilds
Pkg-Description: live pattern based MIDI sequencer
 .
 The Non DAW Studio is a modular system composed of four main parts consisting
 of the Non Timeline, the Non Mixer, the Non Sequencer, and the Non Session
 Manager.
 .
 The Non DAW Studio is a complete studio that is fast and light enough to run
 on low-end hardware.
 .
 This is the Non Sequencer package.

Suggests: lmms

Suggests: swami

Depends: milkytracker

Suggests: schism

Suggests: sfarkxtc

Suggests: vmpk

Depends: pmidi

Recommends: goattracker

Depends: jack-midi-clock

Recommends: kmetronome

Depends: midisnoop

Depends: petri-foo

Depends: qmidinet

Depends: rumor

Depends: showq

Depends: qxgedit

Depends: grandorgue
Pkg-Description: Virtual Pipe Organ Software
 GrandOrgue is a sample based pipe organ simulator. When connected to
 MIDI keyboards and an audio system, it can accurately simulate the sound
 of a real pipe organ.
Homepage: http://sourceforge.net/projects/ourorgan/
License: GPL
WNPP: 693140

Depends: giada

timgm6mb-soundfont

Depends: kmid2
Pkg-Description: MIDI/Karaoke player for KDE
 KMid is a rewrite from scratch of the original KDE midi player.
 It plays MIDI and karaoke files.
 MIDI sequencing is implemented on pluggable backends.
Homepage: http://kmid2.sourceforge.net/
License: GPL
WNPP: 578750

Recommends: machina
Pkg-Description: polyphonic MIDI sequencer based on finite-state automata
 Machina is probabilistic (i.e. very small machines can produce interesting
 non-repetitive output) but also capable of deterministically representing
 any piece of “discrete note music” - or anything in between. There is also
 experimental support for evolving machines (using a Genetic Algorithm) to
 play similar (but not identical) music to an existing piece (from a MIDI
 file or real-time MIDI instrument input).
Homepage: http://www.drobilla.net/software/machina/
License: GPL
WNPP: 629522

Depends: mma

Depends: gsequencer

Depends: midicsv

Depends: superboucle
Pkg-Description: Loop based software fully controllable with any midi device
 SuperBoucle is a loop based software fully controllable with any midi device.
 SuperBoucle is also synced with jack transport. You can use it on live
 performance or for composition.
 .
 SuperBoucle is composed of a matrix of sample controllable with external
 midi device like pad. SuperBoucle will send back information to midi
 device (light up led). Sample will always start and stop on a beat or
 group of beats. You can adjust duration of sample (loop period) in beat
 and offset in beat. But you can also adjust sample offset in raw frame
 count negative or positive. Which mean sample can start before next beat
 (useful for reversed sample). You can record loop of any size, adjust
 BPM, reverse, and normalize samples.
Homepage: http://superboucle.nura.eu/
License: GPL 3
WNPP: 798580

Depends: linuxband
Pkg-Description: GUI front-end for MMA (Musical MIDI Accompaniment)
 LinuxBand is a GUI front-end for MMA (Musical MIDI Accompaniment). Type
 in the chords, choose the groove and LinuxBand will play a musical
 accompaniment for you.
 .
 It’s an open source alternative to Band-in-a-Box featuring:
 .
 * Easy to use graphical interface
 * Open and well-documented data format
 * Output to JACK Midi to facilitate co-operation with other audio
   applications
Homepage: http://linuxband.org/
License: GPL-2+
WNPP: 799331

Depends: openoctave
Pkg-Description: MIDI and audio sequencer and musical notation editor
 Open Octave Midi (OOMIDI) is a fork of Rosegarden 1.7.3, concentrating
 on Midi matrix, event, and matrix percussion editing, and the
 development of keystroke and mouse based workflow with Jackmidi
 intended as it's port device base for predominately orchestral
 composers and writers.
Homepage: http://www.openoctave.org/
License: GPL-2+
WNPP: 578787
Vcs-Git: http://git.debian.org/?p=pkg-multimedia/oom.git

Depends: beast

Suggests: dsmidiwifi

Suggests: playmidi

Suggests: osc2midi
Pkg-Description: Highly flexible and configurable OSC / JACK MIDI bridge
 It was designed especially for use on Linux desktop and the open source
 Android app called "Control (OSC+MIDI)" but was deliberately written
 to be flexible enough to be used with any OSC controller or target.
Homepage: https://github.com/ssj71/OSC2MIDI
License: GPL-3
WNPP: 942333

Suggests: vguitar
Pkg-Description: Play Guitar in any term w MIDI synthesizer
 Vguitar is a MIDI instrument Guitar and is a tablature editor and can easily read                                         
 existing ASCII tablature with some minor editing. Connect via ALSA to a synthesizer.                                      
 Vguitar supports a six string guitar with standard and alternative tunings including                                      
 relative, MIDI and Drop D tunings, supports box and strum modes.
Homepage: http://www.nick-strauss.com/
License: GPL
WNPP: 970653    

Recommends: osmid

Suggests: octave-audio
