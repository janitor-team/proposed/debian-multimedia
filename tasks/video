Task: Video
Description: Video packages
 This metapackage will install Video software. Some are especially useful
 to combine video and sound.
Install: true

Depends: xjadeo

Depends: blender

Depends: lives

Depends: openshot

Depends: openshot-docs
Pkg-Description: Help manual for OpenShot Video Editor
 OpenShot Video Editor is a free, open-source, non-linear video editor,
 based on Python, GTK+, and MLT. It can edit video and audio files,
 composite and transition video files, and mix multiple layers of video
 and audio together and render the output in many different formats.
Homepage: http://www.openshotvideo.com/
License: GPL-3
WNPP: 569773
Pkg-URL: https://code.launchpad.net/~openshot.developers/openshot/openshot-docs-debian

Depends: openshot-qt

Depends: ffmpeg

Depends: ffmpeg2theora

Depends: gjacktransport

Depends: liba52-0.7.4

Suggests: kdenlive

Suggests: dvgrab

Suggests: freemix
Pkg-URL: http://freemix.forja.rediris.es/index.html
License: GPL-2
Pkg-Description: is live video performance software
 .
 Freemix is in beta stage. The aim is to develop a tool that any videoartist
 can adapt to. Freemix provides a videosources table and a sequencer to mix
 videos live while handling the sequencer bpm, video pitch and much more.

Suggests: kino

Suggests: stopmotion

Suggests: subtitleeditor

Depends: advene

Depends: camorama

Suggests: create-resources

Depends: crtmpserver

Depends: gstreamer1.0-crystalhd

Depends: devede

Depends: dirac

Depends: dvbcut

Depends: dvd-slideshow

Depends: dvdwizard

Depends: ffdiaporama

Depends: ffmsindex

Depends: flowblade

Depends: flumotion

Depends: freetuxtv

Depends: frei0r-plugins

Depends: gnome-mplayer

Depends: gpac

Depends: gscanbus

Depends: handbrake

Depends: harvid

Recommends: i965-va-driver

Depends: m2vrequantiser

Depends: mediatomb

Depends: mjpegtools-gtk, mjpegtools

Depends: mp4v2-utils

Depends: mpeg2dec

Recommends: mplayer, mplayer-gui, mencoder
WNPP: 763826

Depends: mpv

Depends: multicat

Depends: browser-plugin-vlc

Depends: ogmrip, ogmrip-plugins

Depends: rtmpdump

Depends: shotdetect

Depends: smplayer-themes

Depends: smtube

Depends: transcode

Depends: tsdecrypt

Recommends: vdpau-va-driver

Depends: videolan-doc

Depends: videotrans

Depends: x264

Suggests: kodi

Suggests: kodi-eventclients-kodi-send,
         kodi-eventclients-ps3, kodi-eventclients-wiiremote

Suggests: kodi-pvr-argustv

Suggests: kodi-pvr-dvbviewer

Suggests: kodi-pvr-hdhomerun

Suggests: kodi-pvr-hts

Suggests: kodi-pvr-iptvsimple

Suggests: kodi-pvr-mediaportal-tvserver

Suggests: kodi-pvr-mythtv

Suggests: kodi-pvr-nextpvr

Suggests: kodi-pvr-njoy

Suggests: kodi-pvr-vdr-vnsi

Suggests: kodi-pvr-vuplus

Suggests: kodi-pvr-wmc

Depends: opencollada-tools

Depends: x265

Depends: vdr-plugin-vnsiserver

Suggests: ffmulticonverter
Pkg-Description: graphical multi format converter
 FF Multi Converter is a simple graphical application which
 enables the user to convert audio, video, image and document
 files between all popular formats by using and combining other
 programs. It uses avconv for audio/video files, unoconv for
 document files and ImageMagick for image file conversions.
 . 
 The goal of FF Multi Converter is to gather the most popular
 multimedia types in one application and provide conversion
 options for them easily through a user-friendly interface.
WNPP: 780041
License: GPL-3

Depends: gstreamer-sharp-1.0
Pkg-Description: New GI-based bindings to GStreamer 1.x
 GStreamer-Sharp are C# bindings to GStreamer. This version of the bindings
 has been written from scratch, mainly by Stephan Sundermann in GSoC, based
 on bindinator (a new GObject-Introspection-based code-generation approach).
 This version of the bindings is a "preview" (0.99.0) which means it will be
 API-unstable until its final version (1.0), and it targets GStreamer 1.0
 API. Soname of the bound library is libgstreamer-1.0.so.0.
 .
 Banshee v2.9.1 is the first version which consumes this binding, for
 example.
WNPP: 742729

Depends: libgstreamer-java
Pkg-Description: a set of java bindings for the gstreamer framework.
 In spite of being described by upstream authors as
 unofficial/alternative, gstreamer-java is currently the java binding
 available at gstreamer language bindings  page [1].
 [1] http://gstreamer.freedesktop.org/bindings/
License: LGPL v3.
WNPP: 576984

Depends: mediagoblin

Depends: qyledl
Pkg-Description: GUI for yle-dl to download videos from Yle Areena
 This application is a graphical front-end for yle-dl, made with Qt.
 Yle-dl is a command line application to download videos, sound files,
 and streams from YLE Areena and other YLE on-line services.
 .
 qyledl is based on yle-downloader-gui
 (http://mpartel.github.io/yle-downloader-gui/).
License: GPL-2
WNPP: 746649

Depends: toutv
Pkg-Description: Tou.tv console application
 Tou.tv console application allows searching, listing and downloading
 of TV show episodes hosted on Tou.tv, a streaming service maintained
 by Radio-Canada, the french-language public broadcaster.
License: MIT/X
WNPP: 738275

Depends: yle-dl
Pkg-Description: downloader of media files from YLE
 Yle-dl is a command-line program for downloading media files
 from the two video streaming services of the Finnish national
 broadcasting company Yle: Yle Areena, http://areena.yle.fi/, and Elävä
 Arkisto, http://www.yle.fi/elavaarkisto/.
License: GPLv2
WNPP: 746645

Depends: 2mandvd
Pkg-Description: simple DVD-Video creator
 2ManDVD is the successor of ManDVD, an application for creating
 video DVDs from a wide variety of video formats. Using this
 application, one can also create eye-pleasing menus with video,
 audio, and chapters.
 .
 2ManDVD can import all video formats supported by mencoder.
 .
 This package contains the main executable.
License: GPL
Homepage: http://2mandvd.tuxfamily.org/
WNPP: 690593
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-multimedia/2mandvd.git
Vcs-Git: git://anonscm.debian.org/pkg-multimedia/2mandvd.git

Depends: animata
Pkg-Description: real-time animation software
 Animata is a real-time animation software, designed to create
 interactive background projections for concerts, theatre and dance
 performances, and promotional screenings.
 .
 The peculiarity of the software is that the animation - the movement
 of the puppets, the changes of the background - is generated in
 real-time, making continuous interaction possible. This ability also
 permits that physical sensors, cameras or other environmental
 variables can be attached to the animation of characters, creating a
 cartoon reacting to its environment. For example, it is quite simple
 to create a virtual puppet band reacting to live audio input, or set
 up a scene of drawn characters controlled by the movement of dancers.
 .
 In contrast with the traditional 3D animation programs, creating
 characters in Animata is quite simple and takes only a few minutes.
 On the basis of the still images, which serve as the skeleton of the
 puppets, we produce a network of triangles, some parts of which we
 link with the bone structure. The  movement of the bones is based on
 a physical model, which allows the characters to be easily moved.
 .
 Animata can be connected with widespread programming environments
 used by multimedia developers and artists in order to make use of the
 possibilities of these applications in the fields of image editing,
 sound analysis, or motion capture.
License: GPL version 3
WNPP: 611044

Depends: avidemux
Pkg-Description: A small editing software for avi (especially DivX)
 With avidemux you can open divx, process the audio track with the included
 filters and save a part of the avi (i.e. split).
 .
 De-multiplex audio & video is also possible, the audio track can be coming
 from the avi or an external Wav/MP3 file.
License: GPL
WNPP: 203211
Homepage: http://fixounet.free.fr/avidemux/

Depends: bombono-dvd
Pkg-Description: DVD authoring program with nice and clean GUI
 Bombono DVD is easy to use program for making DVD-Video.
 The main features of Bombono DVD are:
  * excellent MPEG viewer: Timeline and Monitor
  * real WYSIWYG Menu Editor with live thumbnails
  * comfortable Drag-N-Drop support
  * you can author to folder, make ISO-image or burn directly to DVD
  * reauthoring: you can import video from DVD discs.
Homepage: http://www.bombono.org/
License: GPL
WNPP: 690032
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/bombono-dvd.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/bombono-dvd.git

Depends: booh
Pkg-Description: image classifier and web album generator
 Booh is an image classifier and a static web album generator. It takes
 one or several series of photos and videos, and automatically build
 static web pages to browse through them. The image classifier has all
 the features that one would expect from such software: automatic
 rotation, preloading of images, video support, etc.
Homepage: http://booh.org/
License: GPLv2
WNPP: 518052

Depends: cinelerra-cv
Pkg-Description: non-linear video editor and compositor for Linux.
 Cinelerra, the first Linux based  real-time editing and special effects
 system is a revolutionary Open Source HD media editing system.
 It  has a number of effects built into the system including
 numerous telecine effects,  video special effects including compositing,
 and a complete audio effects system.
 This is a branched version of Cinelerra sometimes called
 Cinelerra-CVS
Homepage: http://git.cinelerra-cv.org/
License: GPL
WNPP: 331072
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/cinelerra-cv.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/cinelerra-cv.git
 
Depends: cinepaint
Pkg-Description: painting and retouching tool for motion pictures
 Cinepaint is primarily used for motion picture frame-by-frame
 retouching and dust-busting. It was used on THE LAST SAMURAI, HARRY
 POTTER and many other films. CinePaint is different from other painting
 tools because it supports deep color depth image formats up to 32 bits
 per channel deep. For comparison, GIMP is limited 8-bit, and Photoshop
 to 16-bit. These debs are built using gtk2.
Homepage: www.cinepaint.org
License: Mix of free licenses
WNPP: 465691

Depends: clipgrab
Pkg-Description: Downloadmanager for many video portals
 Clipgrab is written in QT. You can download videos
 from many video portals like youtube or myvideo.
 It is also possible to convert to various formats.
Homepage: http://clipgrab.de/
License: GPLv3 except logo and name
WNPP: 594792

Depends: colladacoherencytest
Pkg-Description: check the coherency of COLLADA files
 COLLADA Coherency Test is a command-line version of CoherencyTest from
 COLLADA Refinery. It checks whether a COLLADA dae file is coherent
 (properly composed).
 This package ships two versions, built for COLLADA 1.4 and 1.5.
Homepage: http://sourceforge.net/projects/colladarefinery/files/COLLADA%20Coherency%20Test/
License: MIT
WNPP: 589717

Depends: deefuzzer
Pkg-Description: an easy and instant media streaming tool
 DeeFuzzer is an open, light and instant software made for streaming
 audio and video over internet. It is dedicated to media streaming
 communities who wants to create web radios, web televisions, live
 multimedia relaying or even as a personal home radio, with rich media
 contents including track metadata.
Homepage: https://github.com/yomguy/DeeFuzzer
License: GPL
WNPP: 717960

Depends: dvd-vr
Pkg-Description: A program to identify and extract recordings from DVD-VR files
 dvd-vr is a utility to identify and optionally copy recordings from a DVD-VR
 format disc, which can be created by devices like DVD recorders and camcorders.
Homepage: http://www.pixelbeat.org/programs/dvd-vr/
License: GPL
WNPP: 685956

Depends: dvd95
Pkg-Description: DVD9 to DVD5 converter
 dvd95 is a GNOME application to convert DVD9 to DVD5 (4.7GB).
 * Needs no additional packages - embedded versions of vamps and
   dvdauthor are used, to be as fast as possible.
 * Interface is pretty simple to use.
 * Shrinking factor may be computed for best results, or an adaptive
   compression ratio method may be used.
 * DVDs can be converted to file trees or ISOs.
 * The end result can be viewed and burned with regular third party
   players and DVD recording software.
 .
 * DVD95 supports two copy modes:
 - Without menus, one video title set, multiple audio tracks and subtitles.
 - With menus, one video title set, multiple audio tracks and subtitles.
Homepage: http://dvd95.sourceforge.net/
License: GPL
WNPP: 444368
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/dvd95.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/dvd95.git

Depends: dvdstyler
Pkg-Description: DVD authoring and burning tool
Homepage: http://dvdstyler.sf.net/
License: GPL2
WNPP: 635516
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/dvdstyler.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/dvdstyler.git

Depends: djv-imaging
Pkg-Description: professional movie playback software for film production
 This is a video and image sequence playback application. Really its strong
 suit IMO is playing sequences of images which are of EXR or other formats
 typically used in the film industry. There are some image viewers with this
 focus out there but they usually focus on viewing single images. This also
 has some image conversion capabilities as well.
 .
 The upstream developer does build .deb packages, but I did not find a
 source package. I tried to do some work to build it against system
 libraries but it looks like they are using some later versions of image
 libraries that I wasn't totally sure how to handle.
Homepage: http://djv.sourceforge.net/
License: 3-clause BSD
WNPP: 781409

Depends: ffmpeg-gui
Pkg-Description: ffmpeg frontend
 ffmpegGUI is a frontend to ffmpeg that allow you to set
 most important conversion parameters. You can choose your preferred installed
 preset or make and save your own. You can batch convert multiple videos and
 have a log file in html format, if destination size ratio differ, ffmpegGUI
 will automatically add a padding.
 Written using Qt framework.
Homepage: http://sourceforge.net/projects/ffmpegfrontend/
License: GPL-3
WNPP: 607562

Depends: ffmpeg2dirac
Pkg-Description: convert any file that can be decoded by FFmpeg to Dirac
 ffmpeg2dirac is a utility that converts any file that can be decoded by
 FFmpeg to Dirac muxed with Vorbis (if audio exists) in Ogg. It is
 derived from ffmpeg2theora and uses the same command line interface as
 ffmpeg2theora. By default it uses the dirac-research encoder to encode.
 If Schroedinger is installed, it uses the Schroedinger encoder
 if --use-schro argument is provided in the command line.
Homepage: http://diracvideo.org/
License: GPL-3
WNPP: 505739

Depends: flash-player-flv
Pkg-Description: FLV flash player for websites
 flash-player-flv is a video player for FLV file, intended for diffusion
 on websites. It's highly customizable.
Homepage: http://code.google.com/p/mp3player/
License: MPL 1.1
WNPP: 609109

Depends: flowplayer
Pkg-Description: The video player for the Web
 Flowplayer is an Open Source (GPL 3) video player for the web. Use it to 
 embed video streams into your web pages. Built for site owners, 
 developers, hobbyists, businesses, and serious programmers.
Homepage: http://flowplayer.org/index.html
License: GPL-3
WNPP: 657058

Depends: freshplayerplugin
Pkg-Description: plugin to use pepper flash with firefox
 Flash plugin for linux provided by adobe stopped at version 11.2; for
 chrome/chromium users there is pepperflash plugin but it's not supported by
 firefox/iceweasel/other browsers. This plugin allow to use pepper flash from
 chrome with other browsers (it works not only with iceweasel).
 It's still an alpha version, but it seems to work well.
Homepage: https://github.com/i-rinat/freshplayerplugin
License: MIT
WNPP: 775004

Depends: fuoco-converter
Pkg-Description: Universal audio and video converter
 Fueco is usefull to understand ffmpeg and mencoder. It can convert to .ogg, to
 .wav, to ac3 video, etc...
Homepage: http://fuocotools.byethost13.com/index.php
License: GPL
WNPP: 489783

Depends: fuppes
Pkg-Description: FUPPES is a free multiplatform UPnP (TM) A/V Media Server
 FUPPES is a free multiplatform UPnP (TM) A/V Media Server.
 .
 Although it is not yet fully UPnP compliant it already supports a wide
 range of UPnP MediaRenderers. (see "features" for details)
 fuppes supports on-the-fly transcoding of various audio formats.
Homepage: http://fuppes.ulrich-voelkel.de/index.php
License: GPL
WNPP: 426048

Depends: glc
Pkg-Description: ALSA & OpenGL video capture tool
 glc is an ALSA & OpenGL capture tool for Linux. It consists of a 
 generic video capture, playback and processing library and a set of
 tools built around that library. glc should be able to capture any
 application that uses ALSA for sound and OpenGL for drawing. It is
 still a relatively new project but already has a long list of features.
 .
 glc is inspired by yukon (another, excellent real-time video capture
 tool for Linux) and Fraps (a popular Windows tool for the same
 purpose).
Homepage: https://github.com/nullkey/glc
License: Zlib
WNPP: 627560

Depends: h264enc
Pkg-Description: h.264 video encoding script
 This is "just" a shell script, but it's a shell script that knows about
 every command line video tool available for linux, and can put them
 together in handy combinations to generate nice videos. Guides you
 interactively through all the choices.
Homepage: http://h264enc.sourceforge.net/
License: GPL
WNPP: 490386
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/h264enc.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/h264enc.git

Recommends: ht5streamer
Pkg-Description: Youtube/Dailymotion streamer without need of flashplugin
Homepage: https://github.com/smolleyes/ht5streamer
License: GPL
WNPP: 724956

Depends: kmediafactory
Pkg-Description: Easily create DVD with multiple videos and automatic chapters
 KMediaFactory will automatically create a title menu for every video
 file.  Also, for every video file it will automatically create chapter
 menu with a preview image for each chapter.  The default is one chapter
 every 5 minutes of video, but this can be changed manually.
 .
 KMediafactory is easy to use template based dvd authoring tool. You can 
 quickly create DVD menus for home videos and TV recordings in three
 simple steps.
 .
 Version 0.6 has been ported to QT4.
Homepage: http://aryhma.oy.cx/damu/software/kmediafactory/index.html
License: GPL
WNPP: 467228

Suggests: mtn
Pkg-Description: movie thumbnailer
 movie thumbnailer (mtn) -- saves thumbnails (screenshots) of movie or video
 files to jpeg files. It uses FFmpeg's libavcodec as its engine, so it supports
 all popular codecs, e.g. divx h264 mpeg1 mpeg2 mp4 vc1 wmv xvid, and formats,
 e.g. .3gp .avi .dat .mkv .wmv. mtn is open source software. It should run on
 all operating systems which have gcc, FFmpeg, and GD, for example, Linux and
 Windows.
Homepage: http://moviethumbnail.sourceforge.net/
License: GPL
WNPP: 704942

Depends: mythtv
Pkg-Description: A personal video recorder application
 MythTV is a PVR (Personal Video Recorder) application that implements the
 following, and more, with a unified graphical interface:
 .
 - Basic 'live-tv' functionality. Pause/Fast Forward/Rewind "live" TV.
 - Video compression using RTjpeg or MPEG-4
 - Program listing retrieval using XMLTV
 - Themable, semi-transparent on-screen display
 - Electronic program guide
 - Scheduled recording of TV programs
 - Resolution of conflicts between scheduled recordings
 - Basic video editing
Homepage: http://www.mythtv.org/
License: GPL-2
WNPP: 570611

Suggests: kodiplatform
Pkg-Description: Kodi platform support library -- development files
 Kodi platform support library containing utility functions for
 accessing XML files.
Homepage: https://github.com/xbmc/kodi-platform
License: GPL-2+
WNPP: 798489

Depends: noad
Pkg-Description: A tool for automatic marking advertising in VDR-recordings
 Noad means no advertising!
 .
 Noad is a program, that can detect advertising by channellogos,
 and sets cutting marks to remove advertising in VDR-recordings.
 It can be called directly, or can be used as a command for VDR.
Homepage: http://noad.heliohost.org/
License: GPL-2
WNPP: 597484

Depends: gnome-mpv

Depends: phonon-backend-mplayer
Pkg-Description: mplayer backend for phonon
 This package should provide the mplayer backend for phonon, like the
 gstreamer and the xine ones.
Homepage: http://websvn.kde.org/trunk/playground/multimedia/phonon-backends/mplayer/
License: GPL
WNPP: 572788

Depends: pixelstruct
Pkg-Description: structure-from-motion visualizer for 3D scenes
 pixelstruct provides a structure-from-motion visualizer for 3D scenes
 reconstructed from photographs using Bundler.
Homepage: http://da.vidr.cc/projects/pixelstruct/
License: GPL
WNPP: 606817

Suggests: qmagneto
Pkg-Description: TV Electronic Program Guide
 QMagneto is an EPG (Electronic Program Guide) compatible with
 XMLTV files which displays the TV programs. It also able to record programs
 by call an external program as VLC or mencoder. It is thus possible to
 record programs from a HTTP or RTSP stream, or a DVB-T device.
Homepage: http://biord-software.org/qmagneto/
Pkg-URL: Launchpad: https://launchpad.net/~qmagneto
WNPP: 603806

Suggests: qmplay2
Pkg-Description: a video player
 QMPlay2 is a video player, it can plays all formats and stream
 supported by ffmpeg and libmodplug (including J2B). It has integrated
 Youtube browser.
Homepage: http://zaps166.sourceforge.net/?app=QMPlay2
License: LGPL
WNPP: 739417

Suggests: qwinff

Depends: mediaconch

Depends: nordlicht

Depends: castnow
Pkg-Description: command-line chromecast player
 Castnow is a command-line utility that can be used to play back media
 files on your Chromecast device. It supports playback of local video
 files, YouTube clips, videos on the web and torrents. You can also
 re-attach a running playback session.
Homepage: https://github.com/xat/castnow
License: MIT
WNPP: 813736

Depends: baka-mplayer
Pkg-Description: A libmpv based media player
 Baka MPlayer is a free and open source, cross-platform, libmpv based multimedia
 player. Its simple design reflects the idea for an uncluttered and enjoyable
 environment for watching tv shows. Features:
 .
 * Gesture seeking.
 * Smart playlist.
 * Dim Desktop.
 * Hardware accelerated playback (vdpau, vaapi, vda).
 * Youtube playback support (and others).
 * Multilingual support (we are looking for translators!).
 * And more...
Homepage: http://bakamplayer.u8sand.net/
License: GPL-2
WNPP: 813591
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/baka-mplayer.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/baka-mplayer.git

Depends: smoviecat
Pkg-Description: scan for movies, query imdb and generate a catalog
 Scan a given directory for movies, query imdb for info and generate a catalog
 as a html page which offers sorting, filtering and grouping of found movies
 by different criteria.
Homepage: http://smoviecat.sourceforge.net/
License: GPL-3+
WNPP: 822326

Depends: voctomix

Depends: tkmpeg
Pkg-Description: Tk MPEG1 encoder
 This package is a spin-off from saods9 which was separated by
 upstream into its own git repository. It is based on ezMPEG, which
 had its last release in 2012.
Homepage: https://github.com/SAOImageDS9/tkmpeg
License: GPLv2
WNPP: 825061

Suggests: vamps

Suggests: libyami-utils

Depends: gerbera

Depends: tubedown
Pkg-Description: Download videos for mp4 files using Youtube-dl GUI
 Graphical youtube-dl download mp4 (Python 3).
 The tubedown uses the youtube-dl software as the download standard.
 It provides ease for the user, download any video from youtube with simplicity.
 The generated file format will be an .mp4 file.
 Preserving quality and speed.
Homepage: https://github.com/gnewlinux/python/tree/master/tubedown
License: GPL-3
WNPP: 871245

Suggests: xine-console, xine-ui

Suggests: uvccapture

Suggests: streamlink

Suggests: vokoscreen, vokoscreen-ng

Recommends: shotcut

Suggests: dvbtune

Recommends: dvbstreamer

Suggests: haruna

Recommends: lebiniou
