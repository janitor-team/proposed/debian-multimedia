Task: Musician
Description: Packages for musicians
 This metapackage will install packages useful for musicians.
 It includes metronomes, instrument tuners and music notation software.
Install: true

Depends: solfege

Depends: klick

Depends: gtklick

Suggests: fmit

Suggests: stretchplayer

Suggests: impro-visor
Pkg-Description: Jazz Improvisation Advisor for the Improviser
 Impro-Visor (short for “Improvisation Advisor”) is a music notation
 program designed to help jazz musicians compose and hear solos similar
 to ones that might be improvised. The objective is to improve
 understanding of solo construction and tune chord changes. There are
 other, secondary, things it can do, such as improvise on its own. It
 has also been used for transcription. Because rhythm-section
 (e.g. piano, bass, drums) accompaniment is automatically generated from
 chords, Impro-Visor can be used as a play-along device. Now having a
 wider array of accompaniment styles, its use is not limited to jazz. 
Homepage: https://www.cs.hmc.edu/~keller/jazz/improvisor/
License: GPL-2
Pkg-URL: https://launchpad.net/~kxstudio-team/+archive/ubuntu/ppa/+packages?batch=75&memo=75&start=75

Depends: musescore3, musescore # musescore is in buster 

Depends: lilypond

Depends: lilypond-data

Suggests: nted

Suggests: frescobaldi

Suggests: denemo

Suggests: rosegarden

Suggests: mma

Depends: kmetronome

Depends: laborejo

Recommends: rumor

Depends: audiveris
Pkg-Description: Optical Music Recognition module
 Audiveris is an Optical Music Recognition (OMR) module. Starting from 
 the image of a music sheet, it provides high-level logical music 
 information compliant with the MusicXML definition. Other tools such as 
 a Midi Sequencer, or a Composition Editor can then read and update this 
 standard data.
 .
 There are already commercial tools in this area but Audiveris is, to our 
 knowledge, the first Java open-source OMR tool. It is a cross-platform 
 tool, written entirely in Java, and tested on Windows, Solaris, Linux 
 and Mac OS.
 .
 Audiveris works with printed music sheets only, the task of recognizing 
 hand-written scores being significantly harder.
License: GPL
WNPP: 547671

Recommends: bmc
Pkg-Description: Braille Music Compiler
 BMC aims to become a system for translating between visual and tactile
 western music notation.
 It currently parses a subset of braille music code and can emit LilyPond
 input files from that.  Export to other formats like MusicXML is
 planned.
 Eventually, BMC should also be able to convert from MusicXML to braille
 music code.
 .
 It is intended as a bridge between visual and tactile music notation,
 allowing for more and better cooperation between sighted and visually
 handicapped musicians.
 .
 There is no official upstream release yet, the code is still only
 available through github.
Homepage: https://github.com/mlang/bmc
License: GPLv3
WNPP: 681499

Recommends: ly2video
Pkg-Description: generating videos from LilyPond projects
 Ly2video is a Python script for GNU LilyPond and it's able to generate
 video from user's project. Videos contains moving music staff that is
 synchronized with playing audio.
Homepage: https://github.com/aspiers/ly2video/
License: GPL
WNPP: 693307

Suggests: lyricue
Pkg-Description: The GNU Lyric Display System
 This application is used to edit/display song lyrics on a second
 screen/projector for use at singing events such as church services.
 Features
 * Spellchecking
 * User access controls
 * Networkable (ie run interface and server on different machines)
 * Multiple Playlists
 * Copyright info for songs
 * Automatic Page advance
 * Re-orderable playlist
 * Playlist entries to change background
 * All songs kept in a database and so screens are dynamically generated,
   allowing you to easily change the backdrop, font etc without having
   to change all the songs
 * Can automatically create screens for bible verses
 * Quick searching for songs
 .
 Note: lyricue version 1.9.8 is currently present in Ubuntu Intrepid
 Ibex (8.10) [1].
Homepage: http://lds.sourceforge.net/
License: GNU General Public License
Pkg-URL: https://launchpad.net/ubuntu/+source/lyricue
WNPP: 501734

Recommends: mingus
Pkg-Description: advanced music theory and notation package
 mingus is an advanced music theory and notation package 
 for Python with MIDI playback support. It can be used to play 
 around with music theory, to build editors, educational tools and 
 other applications that need to process and/or play music. 
 It can also be used to create sheet music with LilyPond 
 and do automated musicological analysis on chords, scales and 
 harmonic progressions.
 .
 The MIDI package can save and load MIDI files, and -last but not least-
 provides a general purpose sequencer for all the containers and a
 FluidSynth sequencer subclass. This allows you to play all your data
 structures straight from Python in just a couple of lines. Most of the
 icky timing and MIDI code has been abstracted away for you, leaving a
 clean, relatively simple API.
 .
 Lastly, the extra package includes a LilyPond exporter which can be used
 to create sheet music in PDF, PNG and postscript. It also offers ASCII
 tablature and MusicXML exporting and a sound analysis module which can
 recognize notes and melody in raw audio data.
Homepage: https://github.com/bspaans/python-mingus
License: GPL
WNPP: 561615

Suggests: linuxband
Pkg-Description: GUI front-end for MMA (Musical MIDI Accompaniment)
 LinuxBand is a GUI front-end for MMA (Musical MIDI Accompaniment). Type
 in the chords, choose the groove and LinuxBand will play a musical
 accompaniment for you.
 .
 It’s an open source alternative to Band-in-a-Box featuring:
 .
 * Easy to use graphical interface
 * Open and well-documented data format
 * Output to JACK Midi to facilitate co-operation with other audio
   applications
Homepage: http://linuxband.org/
License: GPL-2+
WNPP: 799331

Suggests: qrest
Pkg-Description: set of tools for calculations on musical values
 Qrest is a toolbox aimed at music production people. It is a GUI application
 written in C++ and using the Qt libraries.
 .
 It helps guessing the tempo of a musical piece, and getting the period in 
 millisecond (and related frequency) of the most common notes durations. It 
 also handles common note variations like dotted notes and triplets.
Homepage: http://www.qrest.org/
License: GPL-3
WNPP: 507723

Depends: lute-tab
Pkg-Description: Typesetter for lute tablature
 Tab is a typesetter for lute tablature for renaissance and baroque
 lutes and theorboes, in both French and Italian notation. You edit a
 plain text file with special commands to enter the lute tablature, then
 you run tab to convert that input into PostScript output that you can
 print or display with the right program.
Homepage: https://github.com/mandovinnie/Lute-Tab
License: BSD
WNPP: 825785

Depends: ableton-link-utils, ableton-link-utils-gui

Recommends: qabc
Pkg-Description: minimal GUI for ABC music notation
 QAbc is a simple graphical program that allow
 to write musical scores in the ABC notation.
 .
 This program allow to play the written music,
 preview the output score and print it.
License: GPL3+
Homepage: http://brouits.free.fr/qabc/
WNPP: 975688

Suggests: komposter
Pkg-Description: lightweight music composing system
 This is a lightweight music composing system intended mainly to be used
 in applications where the size of the executable must be minimized such 
 as 4K and 64K intros.
 .
 It is built using a modular "virtual analog" model, where the composer 
 can build the synthesizers from scratch using simple basic building
 blocks. This minimizes the amount of code required and relies more on
 data, which can be compressed more effectively.
 .
 A simple pattern-based sequencer is used to create songs which use up 
 to 24 voices, each of which can use a different synthesizer.
 Each synthesizer can be programmed with a number of patches that can be
 switched between patterns.
 .
 Included with Komposter is a music player with full x86 assembly source 
 code as well as a converter for generating nasm-includeable files from
 song files.
License: GPL-2+ and MIT
Homepage: http://komposter.haxor.fi/
WNPP: 977612
