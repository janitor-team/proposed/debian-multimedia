Task: Broadcasting
Description: Packages for broadcasting media publicly or in the home
 Metapackage which installs packages for broadcasters, cinemas, or home
 users (e.g. streaming & media servers).
Install: true

Depends: asdcplib
Pkg-Description: implementation of SMPTE and the MXF Interop "Sound & Picture Track File" format
 asdcplib is an open source implementation of SMPTE and the MXF Interop
 "Sound & Picture Track File" format. It was originally developed with
 support from DCI. Development is currently supported by CineCert and
 other D-Cinema manufacturers. See the bundled README file for more
 information. asdcplib is implemented in C++ and builds on Win32 and
 most unix platforms that support GCC.
 .
 The following SMPTE standards (and their normative references) are supported:
 .
 377M-2004
 381M-2005
 382M-2007
 429-3-2006
 429-4-2006
 429-5-2008
 429-6-2006
 429-10-2008
 .
 asdcplib supports reading and writing MXF files containing sound
 (PCM), picture (JPEG 2000 or MPEG-2) and timed-text (XML) essence.
 Plaintext and ciphertext are both supported using OpenSSL for
 cryptographic support. An object-oriented API is provided along with a
 command-line program asdcp-test that provides access to most of the
 API.
Homepage: http://www.cinecert.com/asdcplib/
License: BSD 3 Clause
Pkg-URL: https://launchpad.net/~tim-klingt/+archive/opencinematools
WNPP: 564824

Depends: camorama

Depends: crtmpserver

Depends: dcpomatic
Pkg-Description: Digital Cinema Packages (DCPs) from videos, images and sound files
 .
 DCP-o-matic is a free, open-source program to create Digital Cinema Packages
 (DCPs) from videos, images and sound files. This means that you can make
 content to play on DCI-compliant cinema projectors.
 .
 It can take files in many different formats, including MP4, Apple ProRes, MOV,
 AVI, VOB (from DVDs), M2TS (from Blu-Ray), WMV, MKV, JPEG, PNG, TIFF and lots
 of others.
Homepage: http://dcpomatic.com/
License: GPL-2
WNPP: 811556

Depends: dcp-inspect
Pkg-Description: deep inspection and validation of digital cinema packages
 Dcp_inspect is a tool for deep inspection and validation of digital cinema
 packages (DCP, SMPTE and Interop). This includes integrity checks, asset
 inspection, schema validation, signature and certificate verification and
 composition summarization.
Homepage: https://github.com/wolfgangw/backports
License: GPL-3
WNPP: 811562

Depends: dvblast

Depends: flumotion

Depends: forked-daapd

Depends: groovebasin

Depends: icecast2

Depends: ices2

Depends: idjc

Suggests: kodi

Suggests: kodi-eventclients-kodi-send,
         kodi-eventclients-ps3, kodi-eventclients-wiiremote

Suggests: kodi-pvr-argustv

Suggests: kodi-pvr-dvbviewer

Suggests: kodi-pvr-hdhomerun

Suggests: kodi-pvr-hts

Suggests: kodi-pvr-iptvsimple

Suggests: kodi-pvr-mediaportal-tvserver

Suggests: kodi-pvr-mythtv

Suggests: kodi-pvr-nextpvr

Suggests: kodi-pvr-njoy

Suggests: kodi-pvr-vdr-vnsi

Suggests: kodi-pvr-vuplus

Suggests: kodi-pvr-wmc

Suggests: kodi-inputstream-adaptive

Suggests: kodi-inputstream-ffmpegdirect

Suggests: kodi-inputstream-rtmp

Depends: mediatomb

Depends: multicat

Depends: tsdecrypt

Depends: vdr-plugin-vnsiserver

Suggests: videolan-doc

Suggests: vlc

Depends: airtime
Pkg-Description: A radio program automation and support tool
 Campcaster is the first free and open radio management software that 
 provides live studio broadcast capabilities as well as remote automation
 in one integrated system.
 .
 Major features of Campcaster: live, in-studio playout; web-based remote
 station management; automation; playlists; centralized archives of station
 program material; solid, fast playback using the Gstreamer multimedia
 framework; search-based backup; localization into several languages;
 innovative design by the Parsons School of Design; open, extensible
 architecture based on XML-RPC.
License: GPL
Homepage: http://campcaster.campware.org/
WNPP: 568530

Depends: aras
Pkg-Description: open source radio automation system
 ARAS is an open source radio automation system, it manages
 and plays transmissions, shows, music and jingles in radio stations.
License: GPL
Homepage: http://aras.sourceforge.net/
WNPP: 634072

Depends: mediagoblin

Suggests: gallery3
Pkg-Description: web-based photo album written in PHP
 Gallery3 is a web-based photo album with multiple user support.  It
 provides users with the ability to create and maintain their own albums
 via an intuitive web interface.
 .
 Gallery3 is a complete rewrite of the popular Gallery software that
 strives for a simpler codebase and reduced scope to ease
 maintainability.
Homepage: http://gallery.sf.net/
License: GPL
WNPP: 511715

Suggests: photoshow
Pkg-Description: A free Web Gallery for your server
 PhotoShow is a free, open-source, and very easy to use web gallery installable
 on any web server. Only two lines of configuration, and you're good to go !
 Adding and sorting photos, albums, users, and groups have never been easier.
 Simply drag and drop them wherever you want. To delete a photo or an album,
 just drag and drop it on the bin. Easy ! PhotoShow automatically generates
 the whole website, including the thumbnails. Also, if your photos are bigger
 than 800x600px, it generates a smaller version for displaying (the big
 version is still available for your viewers to download, of course). The
 architecture of PhotoShow's albums is created using the File System directly:
 no database is required to use PhotoShow. You can use your File System
 directly to add, move, or delete albums and pictures. PhotoShow is compatible
 with DropBox, along with all of your Unix tools, because it is based on the
 File System. Use rsync, scp, or anything you prefer to interact
 with your photos. Each of your files and folders has its owns permissions,
 configurable in PhotoShow by the Admin (you). On the go, select which
 group/user can access an item. PhotoShow provides a proxy for accessing the
 images, checking if the user is allowed to view an image or not. It is
 advised to place the directory for your photos in a folder not accessible by
 the web. Then, passing through PhotoShow's proxy will be necessary to access
 the images, ensurring you that only allowed users can view them.
License: GPL
Homepage: http://www.photoshow-gallery.com/
WNPP: 664742

Depends: deefuzzer
Pkg-Description: an easy and instant media streaming tool
 DeeFuzzer is an open, light and instant software made for streaming
 audio and video over internet. It is dedicated to media streaming
 communities who wants to create web radios, web televisions, live
 multimedia relaying or even as a personal home radio, with rich media
 contents including track metadata.
Homepage: https://github.com/yomguy/DeeFuzzer
License: GPL
WNPP: 717960

Depends: fuppes
Pkg-Description: FUPPES is a free multiplatform UPnP (TM) A/V Media Server
 FUPPES is a free multiplatform UPnP (TM) A/V Media Server.
 .
 Although it is not yet fully UPnP compliant it already supports a wide
 range of UPnP MediaRenderers. (see "features" for details)
 fuppes supports on-the-fly transcoding of various audio formats.
Homepage: http://fuppes.ulrich-voelkel.de/index.php
License: GPL
WNPP: 426048

Depends: celtx
Pkg-Description: media pre-production software
 Celtx is a comprehensive software package designed for
 people who work in the film, TV, theatre, and new media
 industries. It combines full-feature scriptwriting with
 media rich pre-production support and enables online
 collaboration.
Homepage: http://celtx.com/
License: MPL with amendments
WNPP: 409048

Recommends: ingex
Pkg-Description: tapeless television production software
 Ingex is a suite of software applications designed to enable low-cost
 flexible tapeless television production. The functionality of the software
 includes SDI video and audio capture, real-time transcoding and wrapping in
 MXF, archiving to LTO-3 data tape and network file serving of media files.
 The software was originally developed by BBC Research and Development in
 2005 for series three of the BAMZOOKi BBC children's program and is now
 released under the terms of the GPL software licence. 
 .
 Ingex has since been used by the EastEnders and Dragons' Den television
 productions as well as a music video production for BBC Radio One.
Homepage: http://ingex.sourceforge.net/
License: GPL2
WNPP: 525615

Depends: obs
Pkg-Description: Open Broadcast Software
 OBS is a rewrite of what was formerly known as "Open Broadcaster Software", 
 software originally designed for recording and streaming live
 video content, efficiently.
License: GPL
Homepage: https://obsproject.com/
WNPP: 774744
Remark: In NEW queue - binaries will be obs-plugins & obs-studio

Suggests: kodiplatform
Pkg-Description: Kodi platform support library -- development files
 Kodi platform support library containing utility functions for
 accessing XML files.
Homepage: https://github.com/xbmc/kodi-platform
License: GPL-2+
WNPP: 798489

Depends: nuvolaplayer
Pkg-Description: cloud music integration for Linux desktop
 Nuvola Player runs web interface of cloud music service
 in its own window and provides integration with Linux
 desktop (multimedia keys, system tray, notifications,
 MPRIS interface, Ubuntu sound menu, Unity Launcher Quick
 List and other dock menus). Currently supported services
 are Google Music, Grooveshark, Hype Machine and 8tracks.
Homepage: http://projects.fenryxo.cz/Nuvola_Player/
License: GPL-3+
WNPP: 653701

Depends: opencinematools
Pkg-Description: tools to create a Digital Cinema Package (DCP)
 Tools to create a Digital Cinema Package (DCP) from MXF source files
 according to the DCI-specification for use in movie theaters.
Homepage: http://code.google.com/p/opencinematools/
License: New BSD
WNPP: 564823
Pkg-URL: https://launchpad.net/~tim-klingt/+archive/opencinematools

Depends: opendcp
Pkg-Description: Open Digital Cinema Package software
 OpenDCP is an open source program to create digital cinema packages
 (DCP) for use in digital cinema.
 .
 Features:
 * JPEG2000 encoding from 8/12/16-bit TIFF/DPX RGB/YUV/YCbCr images
 * Supports all major frame rates (24,25,30,48,50,60)
 * Cinema 2K and 4K
 * MPEG2 MXF
 * XYZ color space conversion
 * MXF file creation
 * SMPTE and MXF Interop
 * Full 3D support
 * DCP XML file creation
 * SMPTE subtitles
 * Linux/OSX/Windows
 * Multithreaded for encoding performance
 * XML Digital signatures
 * GUI
Homepage: https://github.com/tmeiczin/opendcp
License: GPL
WNPP: 655338

Depends: phonon-backend-mplayer
Pkg-Description: mplayer backend for phonon
 This package should provide the mplayer backend for phonon, like the
 gstreamer and the xine ones.
Homepage: http://websvn.kde.org/trunk/playground/multimedia/phonon-backends/mplayer/
License: GPL
WNPP: 572788

Depends: pipelight
Pkg-Description: load windows NPAPI plugins into linux browsers
 A project to load windows NPAPI plugins into linux browsers using wine
 and a pipe.
 Today we want to present you our latest project Pipelight, which allows to run
 your favorite Silverlight application directly inside your Linux browser. The
 project combines the effort by Erich E. Hoover with a new browser plugin that
 embeds Silverlight directly in any Linux browser supporting the Netscape Plugin
 API (Firefox, Chrome / Chromium, Midori, Opera, …). He worked on a set of
 Wine patches to get Playready DRM protected content working inside Wine and
 afterwards created an Ubuntu package called Netflix Desktop. This package
 allows one to use Silverlight inside a Windows version of Firefox, which works
 as a temporary solution but is not really user-friendly and moreover requires
 Wine to translate all API calls of the browser. To solve this problem we
 created Pipelight.
 .
 Pipelight consists out of two parts: A Linux library which is loaded into the
 browser and a Windows program started in Wine. The Windows program, called
 pluginloader.exe, simply simulates a browser and loads the Silverlight DLLs.
 When you open a page with a Silverlight application the library will send all
 commands from the browser through a pipe to the Windows process and act like a
 bridge between your browser and Silverlight. The used pipes do not have any big
 impact on the speed of the rendered video since all the video and audio data is
 not send through the pipe. Only the initialization parameters and (sometimes)
 the network traffic is send through them.
Homepage: https://bitbucket.org/mmueller2012/pipelight/overview
License: GPL
WNPP: 743140

Depends: plumi
Pkg-Description: video sharing Content Management System based on Plone
 Plumi is a Free Software video sharing Content Management System based
 on Plone and produced by the EngageMedia collective. Plumi enables you
 to create your own sophisticated video sharing site; by adding it to an
 existing Plone instance you can quickly have a wide array of
 functionality to facilitate video distribution and community creation.
 .
 In a net landscape where almost all video sharing sites keep their
 distribution platform under lock and key, Plumi is one contribution to
 creating a truly democratic media.
Homepage: http://plumi.org/
License: GPL
WNPP: 511980

Suggests: ps3-media-server
Pkg-Description: DLNA UPnP Media Server, dedicated to PS3
 PS3 Media Server is a DLNA compliant Upnp Media Server for the PS3,
 written in Java, with the purpose of streaming or transcoding any kind
 of media files, with minimum configuration. It's backed up with the
 powerful Mplayer/FFmpeg packages.
 .
 Current features:
    * Ready to launch and play. No codec packs to install. No folder
      configuration and pre-parsing or this kind of annoying thing.
      All your folders are directly browsed by the PS3, there's an
      automatic refresh also.
    * Real-time video transcoding of MKV/FLV/OGM/AVI, etc.
    * Direct streaming of DTS / DTS-HD core to the receiver
    * Remux H264/MPEG2 video and all audio tracks to AC3/DTS/LPCM in
      real time with tsMuxer when H264 is PS3/Level4.1 compliant
    * Full seeking support when transcoding
    * DVD ISOs images / VIDEO_TS Folder transcoder
    * OGG/FLAC/MPC/APE audio transcoding
    * Thumbnail generation for Videos
    * You can choose with a virtual folder system your audio/subtitle
      language on the PS3!
    * Simple streaming of formats PS3 natively supports:
      MP3/JPG/PNG/GIF/TIFF, all kind of videos (AVI, MP4, TS, M2TS, MPEG)
    * Display camera RAWs thumbnails (Canon / Nikon, etc.)
    * ZIP/RAR files as browsable folders
    * Support for pictures based feeds, such as Flickr and Picasaweb
    * Internet TV / Web Radio support with VLC, MEncoder or MPlayer
    * Podcasts audio/ Video feeds support
    * Basic Xbox360 support
    * FLAC 96kHz/24bits/5.1 support
    * Windows Only: DVR-MS remuxer and AviSynth alternative transcoder
      support
Homepage: http://code.google.com/p/ps3mediaserver/
License: GPLv2
WNPP: 551645

Depends: pulseaudio-dlna

Suggests: qlc
Pkg-Description: application to control DMX or analog lighting systems
 Q Light Controller is a cross-platform application for controlling DMX or
 analog lighting systems like moving heads, dimmers, scanners etc. The goal is
 to replace hard-to-learn and somewhat limited commercial lighting desks with
 FLOSS. 
 .
 Lots of additional DMX interfaces are supported thru the Open Lighting
 Architecture (OLA) on Linux and Mac OS X.
Homepage: http://qlc.sourceforge.net/
License: GPL
WNPP: 694077

Suggests: qmagneto
Pkg-Description: TV Electronic Program Guide
 QMagneto is an EPG (Electronic Program Guide) compatible with
 XMLTV files which displays the TV programs. It also able to record programs
 by call an external program as VLC or mencoder. It is thus possible to
 record programs from a HTTP or RTSP stream, or a DVB-T device.
Homepage: http://biord-software.org/qmagneto/
Pkg-URL: Launchpad: https://launchpad.net/~qmagneto
WNPP: 603806

Depends: voctomix

Depends: gerbera

Depends: dleyna-renderer, dleyna-server

Depends: darkice

Depends: gmediaserver

Suggests: butt

Suggests: snapclient, snapserver

Suggests: dvbstreamer
